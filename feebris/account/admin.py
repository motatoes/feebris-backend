# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import CustomUser

class UserAdmin(admin.ModelAdmin):
    list_display = ('username', 'email')
    fields = ('username',)
    search_fields = ('username',)
    # ordering = ('-created',)
    # autocomplete_fields = ('username',)

admin.site.register(CustomUser, UserAdmin)
