# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib.auth.models import AbstractUser
from django.db import models
from .signals import *

class CustomUser(AbstractUser):
    pass

    def __str__(self):
        return self.email