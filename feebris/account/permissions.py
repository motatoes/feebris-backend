from rest_framework import permissions

class IsOwner(permissions.BasePermission):

    def has_permission(self, request, view):
        if request.user.is_superuser:
            return True
        
        if request.method in permissions.SAFE_METHODS:
            return True

        # check if the request is for a single object
        # if its a detailed request we do our checks in the is_owner_permission class
        if "pk" in view.kwargs:
            return True
        
        return True

    def has_object_permission(self, request, view, obj):
        """
        Object-level permission to only allow owners of an object to edit it.
        Assumes the model instance has an `owner` attribute.
        """
        # Read permissions are allowed to any request,
        # so we'll always allow GET, HEAD or OPTIONS requests.
        # if request.method in permissions.SAFE_METHODS:
        #     return True
        print("i was here")
        if request.user.is_superuser:
            return True

        print(view.kwargs)
        if "pk" not in view.kwargs:
            return True
            
        # check if trying to fetch current user
        if obj.email == request.user.email:
            return True

        return False