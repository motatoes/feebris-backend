# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth import get_user_model
from django.shortcuts import render
from django.contrib.auth.models import Group
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated
from account.serializers import (
    UserSerializer, 
    UserSummarySerializer,
    GroupSerializer
)
from account.permissions import IsOwner

class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    permission_classes = [IsOwner]
    queryset = get_user_model().objects.all().order_by('-date_joined')
    serializer_class = UserSerializer

    def list(self, request):
        self.serializer_class = UserSummarySerializer
        return super(UserViewSet, self).list(request)

class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    permission_classes = [IsAuthenticated,]
    queryset = Group.objects.all()
    serializer_class = GroupSerializer