# Generated by Django 2.2.3 on 2019-07-16 15:00

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('diagnose', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='diagnosis',
            name='result',
            field=models.CharField(choices=[['Pending', 'Pending'], ['Flu', 'Flu'], ['No_Flu', 'No Flu']], default='Pending', max_length=60),
        ),
        migrations.AddField(
            model_name='diagnosis',
            name='status',
            field=models.CharField(choices=[['Pending', 'Pending'], ['Complete', 'Complete'], ['Failed', 'Failed']], default='Pending', max_length=60),
        ),
    ]
