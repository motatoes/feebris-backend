# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.utils import timezone
from django.contrib.auth import get_user_model
from django.db import models

class DiagnosisStatus:
    Pending = "Pending"
    Complete = "Complete"
    Failed = "Failed"

    CHOICES = [
        [Pending, "Pending"],
        [Complete, "Complete"],
        [Failed, "Failed"],
    ]

class DiagnosisResult:
    Pending = "Pending"
    Flu = "Flu"
    No_Flu = "No_Flu"

    CHOICES = [
        [Pending, "Pending"],
        [Flu, "Flu"],
        [No_Flu, "No Flu"]
    ]

class Diagnosis(models.Model):
    patient = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)

    # temperature in centigrade (support for ferenhiet in future ?)
    temperature = models.DecimalField(max_digits=6, decimal_places=2)

    # ever had cough recently
    cough = models.BooleanField()

    # fever in the past 5 days
    fever = models.BooleanField()

    # 
    status = models.CharField(max_length=60, choices=DiagnosisStatus.CHOICES, default="Pending")

    # 
    result = models.CharField(max_length=60, choices=DiagnosisResult.CHOICES, default="Pending")

    date_created = models.DateTimeField(default=timezone.now)