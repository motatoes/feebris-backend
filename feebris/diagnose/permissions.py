from rest_framework import permissions

class DiagnosisPermission(permissions.BasePermission):
    def has_permission(self, request, view):
        if request.user.is_superuser:
            return True
        
        if not request.user.is_authenticated:
            return False

        if request.method in permissions.SAFE_METHODS:
            return True

        if request.method == "POST":
            return True

        # check if the request is for a single object
        # if its a detailed request we do our checks in the is_owner_permission class
        if "pk" in view.kwargs:
            return True


        return False

    def has_object_permission(self, request, view, obj):
        """
        Object-level permission to only allow owners of an object to edit it.
        Assumes the model instance has an `owner` attribute.
        """
        # Read permissions are allowed to any request,
        # so we'll always allow GET, HEAD or OPTIONS requests.
        # if request.method in permissions.SAFE_METHODS:
        #     return True

        if request.user.is_superuser:
            return True

        # The user is trying to add a new diagnosis which is allowed
        if "pk" not in request.views and request.method == "POST":
            return True

        # check if trying to fetch current user
        if obj.patient.email == request.user.email:
            return True

        return False