from rest_framework import serializers
from .models import Diagnosis

class DiagnosisSerializer(serializers.ModelSerializer):
    class Meta:
        model = Diagnosis
        fields = ['url', 'patient', 'temperature', 'cough', 'fever', 'status', 'result']
