# Create your tasks here
from __future__ import absolute_import, unicode_literals
import logging
from celery import shared_task
from .models import Diagnosis, DiagnosisResult, DiagnosisStatus
from .utils import diagnose_patient
logger = logging.getLogger(__name__)

@shared_task
def diagnose_patient_task(diagnosis_id):
    try:
        diagnosis = Diagnosis.objects.get(id=diagnosis_id)
    except Diagnosis.DoesNotExist as e:
        logger.exception("Could not find object with id: " + str(diagnosis_id))        
        raise e

    if diagnosis.fever and \
            diagnosis.cough and \
            diagnosis.temperature > 38:
        diagnosis.result = DiagnosisResult.Flu
    else:
        diagnosis.result = DiagnosisResult.No_Flu
    result = diagnose_patient(diagnosis)
    diagnosis.result = result
    # mark this diagnosis object as complete
    diagnosis.status = DiagnosisStatus.Complete
    diagnosis.save()
