from .models import DiagnosisResult

def diagnose_patient(diagnosis_obj):
    diagnosis = diagnosis_obj
    if diagnosis.fever and \
            diagnosis.cough and \
            diagnosis.temperature > 38:
        return DiagnosisResult.Flu
    else:
        return DiagnosisResult.No_Flu
