# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render
from django.contrib.auth import get_user_model
from django.shortcuts import render
from django.contrib.auth.models import Group
from rest_framework import viewsets, status
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from .permissions import DiagnosisPermission
from .serializers import DiagnosisSerializer
from .models import Diagnosis, DiagnosisStatus
from .tasks import diagnose_patient_task
from .utils import diagnose_patient

class DiagnosisViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    permission_classes = [IsAuthenticated, DiagnosisPermission]
    queryset = Diagnosis.objects.all().order_by('-date_created')
    serializer_class = DiagnosisSerializer

    def create(self, request):
        data = request.data.copy()
        data.pop("patient", None)
        data.pop("status", None)
        data.pop("result", None)
        data["patient"] = request.user.pk
        serializer = self.get_serializer(data=data)
        serializer.is_valid(raise_exception=True)
        diagnosis = serializer.save()
        # diagnose_patient_task.delay(diagnosis.id)     
        result = diagnose_patient(diagnosis)   
        diagnosis.result = result
        diagnosis.status = DiagnosisStatus.Complete
        diagnosis.save()
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def update(self, request, pk=None):
        request.data.pop("patient", None)